import React, {useState} from 'react';
import { View, Text } from "react-native";
import data from '../data/todo.data';
import TodoDisplay from "../component/todo.display";

const TodoPage = () => {

    const [todo, setTodo] = useState(data);

    return(
        <View>

            <Text> Page de todo </Text>
            <TodoDisplay todo={todo}></TodoDisplay>
        </View>
    );

}

export default TodoPage;