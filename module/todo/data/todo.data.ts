import {TodoModel} from "../model/todo.model";

const TodoData: TodoModel[] = [

    new TodoModel('Apprendre react'),
    new TodoModel('passer à react native'),
    new TodoModel('faire un exercice')

]

export default TodoData;