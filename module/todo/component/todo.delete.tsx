import {View, Text, Button} from "react-native";
import React from 'react';
import {TodoService} from "../service/todo.service";

const TodoDelete = (id: any) => {

    return(
        <View>
            <Button title="Delete" onPress={() => TodoService.todoDelete(id)}
            ></Button>
        </View>
    );

}

export default TodoDelete;