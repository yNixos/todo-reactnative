
import { View, Text } from "react-native";
import React, {useState} from 'react';
import {TodoModel} from "../model/todo.model";
import TodoDelete from "./todo.delete";

const TodoDisplay = (props: any) => {

    const [todo, setTodo] = useState(props.todo)

    return(
        <View>

            <ul>

                {todo.map((todo : TodoModel) => {

                    return <li> {todo.task} <TodoDelete id={todo.id}></TodoDelete> </li>

                })}

            </ul>

            <Text> {todo[0].task} </Text>

        </View>
    );

}

export default TodoDisplay;